Clarus
======

Clarus is a Sass-enabled starter theme for building custom responsive Drupal 8 themes. This theme is not meant to be used out of the box. Rather, it serves as a blank slate for projects which require a bespoke theme, thus this starter theme is based on Stark, which removes all classes and is a truly blank canvas.

The Sass files in this theme are loosely based on Harry Roberts' ITCSS methodology and are split into partials. Feel free to add on or change them as you wish.

This theme uses Gulp to compile the Sass files to CSS. It also makes use of [BrowserSync](http://www.browsersync.io/) for live-reloading. 

### Requirements
1. [NodeJS](http://nodejs.org) - You can use the installer, but I personally like using [Homebrew](http://blog.teamtreehouse.com/install-node-js-npm-mac)
2. [GulpJS](http://gulpjs.com) - `npm install -g gulp`
3. [Browsersync](https://www.drupal.org/project/browsersync) - `drush dl browsersync -y` 
    - This Drupal module is required for Browsersync to work. Go to *Appearance > Settings* and check *Enable Browsersync*.

### Getting started
- Clone this repository into your working directory.
- Navigate to the directory and run `yarn install`. This will install the required node modules for the project.

### Running Gulp during theming
- Run `gulp`. This will run the Gulp tasks needed for:
    -  compiling Sass,
    -  clearing cache after changes to `.yml` and `.twig` files
    -  watching for changes to relevant folders and reloading the browser accordingly.
